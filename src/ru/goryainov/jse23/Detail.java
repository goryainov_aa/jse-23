package ru.goryainov.jse23;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Detail {
    public static List<List<Description>> detailDSC(List<Object> objects) throws IllegalAccessException, IllegalArgumentException {
        List<List<Description>> result = new ArrayList<>();
        Class<?> clazz1 = objects.get(0).getClass();
        for (Object object : objects) {
            Class<?> nextClazz = object.getClass();
            if (clazz1 != nextClazz)     {
                throw new IllegalArgumentException("Переданы объекты разного типа!");
            }
            List<Description> fieldsList = new ArrayList<>();
            Field[] fields = nextClazz.getDeclaredFields();
            int l = fields.length;
            for (Field field : fields) {
                if (field.get(object) != null){
                    fieldsList.add(new Description(field.getName(), field.getType().getName(), true));
                }
                fieldsList.add(new Description(field.getName(), field.getType().getName(), false));
            }
            result.add(fieldsList);
        }
        return result;
    }
}
