package ru.goryainov.jse23;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IllegalAccessException {
        List<Object> person = new ArrayList<>();
        person.add(new Person("Ivan", "Ivanov", LocalDate.of(1900, 1, 1)));
        person.add(new Person("Petr", "Petrov", "petrov_p@mail.ru"));
        System.out.println("start");
        for (List<Description> lst : Detail.detailDSC(person)) {
            for (Description dsc : lst) {
                System.out.println(dsc);
            }
            System.out.println("finish");
        }
    }
}
